from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from dotenv import load_dotenv
import time
import os
import sys
import glob

def download(driver, url, username, password):
    driver.get(url)

    # write user name
    element = driver.find_element(By.ID,"loginname").send_keys(username)
    # write password
    element = driver.find_element(By.ID,"password")
    element.send_keys(password)
    element.send_keys(Keys.RETURN)

    # select all documents
    driver.find_element(By.ID,"proxy-checkbox-0").click()
    # hit download
    driver.find_element(By.NAME,"download").click()

    # sleep until download is done
    while not glob.glob(os.path.join(destination, '*.zip')):
        time.sleep(1)

if __name__ == '__main__':
    try: 
        url = sys.argv[1]
        destination = sys.argv[2]

    except Exception as e:
        print(f"Expected <directory cid> and <destination>. Exception: {e}")
        print("exiting")
        exit(0)

    options = webdriver.ChromeOptions()
    prefs = {"download.default_directory" : destination}
    options.add_argument("--headless=new")
    options.add_experimental_option("prefs", prefs)
    driver = webdriver.Chrome(options=options)

    load_dotenv()

    download(driver,
             url,
             os.getenv('STUDIP_USER'),
             os.getenv('STUDIP_PASSWORD')
    ) 
